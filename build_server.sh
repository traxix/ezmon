#/bin/bash -ex

docker build --build-arg GIT_BRANCH_CLONE="dev" --build-arg FORCE_CODE_PULL="$(date +%s)" -f Dockerfile.server -t registry.gitlab.com/traxix/ezmon/server:latest .
docker push registry.gitlab.com/traxix/ezmon/server:latest 
ssh traxix.net "./update_ezmon.sh"
