#!/bin/bash -ex

id=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32 ; echo '')

if [ "$#" -eq 1 ]; then
    id="$1:$id"
fi

echo $id
return

sudo apt install -y collectd
cd /etc/collectd
sudo curl -o ezmon.conf https://gitlab.com/traxix/ezmon/raw/master/ezmon.conf
sudo mv collectd.conf collectd.conf.old || echo "default file not found"
sudo sed "s/^ *Hostname.*/Hostname \"$id\"/" -i ezmon.conf
sudo ln -s ezmon.conf collectd.conf
sudo service collectd restart


echo ""
echo ""
echo ""
echo ""
echo "==============================================================================="
echo ""
echo ""
echo "Monitoring in place, you can visit at:"
echo "https://ezmon.traxix.net/metrics/$id"
echo ""
echo ""
echo "==============================================================================="
